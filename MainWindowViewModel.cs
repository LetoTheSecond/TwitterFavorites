﻿namespace TwitterLikesBrowser {
	using System.ComponentModel;
	using System.Diagnostics;
	using System.Threading;
	using System.Windows.Input;
	using Tweetinvi;

	class MainWindowViewModel: INotifyPropertyChanged {

		private string _UserName;
		public DelegateCommand SearchButton { get; set; }

		public MainWindowViewModel() {
			Auth.SetUserCredentials(
				"sD956GlCAmBKgVSIJs7UohIoN",
				"1pitEmD5Fd1ALXcDXvHjgTHJxvseEhLjJg5JTESCIVHkWnUqJt",
				"213540951-NY59VmEyWDxim42zTGbzpcQ4iRudXxhcZe87BmfO",
				"E8Sel3vSsabWq5hq8oLYYnS686NXVDWwMa5PzmPSNA4Rm");
			_UserName = "";
			SearchButton = new DelegateCommand(_GetUserLikes,_SearchCheck);
		}
		
		public string User {
			set {
				_UserName = value;
				SearchButton.RaiseCanExecuteChanged();
			}
			get { return _UserName; }
		}

		public ICommand Search => SearchButton;
		private bool _SearchCheck() { return !_UserName.Equals(""); }
		private void _GetUserLikes() {
			Debug.WriteLine("Searching for User: "+_UserName);
			var user = Tweetinvi.User.GetUserFromScreenName(_UserName);
			//TODO 
		}

		public event PropertyChangedEventHandler PropertyChanged;
		protected virtual void NotifyPropertyChanged(string propertyName) {
			if(PropertyChanged != null) {
				var p = new PropertyChangedEventArgs(propertyName);
				PropertyChanged(this,p);
			}
		}

	}

}
