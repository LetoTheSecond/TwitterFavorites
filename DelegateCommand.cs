﻿namespace TwitterLikesBrowser {
	using System;
	using System.Windows.Input;

	//class DelegateCommand : ICommand{
	//	private readonly Action _action;

	//	public DelegateCommand(Action action) { _action = action; }

	//	void ICommand.Execute(object parameter){ _action(); }

	//	bool ICommand.CanExecute(object parameter){ return true; }

	//	public event EventHandler CanExecuteChanged { add { } remove { } }

	//}

	public class DelegateCommand: ICommand {
		readonly Action _executeMethod;
		readonly Func<bool> _canExecuteMethod;
		public event EventHandler CanExecuteChanged;

		public DelegateCommand(Action executeMethod) : this(executeMethod,() => true) { }
		public DelegateCommand(Action executeMethod,Func<bool> canExecuteMethod){
			_executeMethod = executeMethod;
			_canExecuteMethod = canExecuteMethod;
		}

		public bool CanExecute(object parameter) => _canExecuteMethod();
		public void Execute(object parameter) => _executeMethod();
		public void RaiseCanExecuteChanged() => OnCanExecuteChanged();
		private void OnCanExecuteChanged() => CanExecuteChanged?.Invoke(this,EventArgs.Empty);

	}

}
