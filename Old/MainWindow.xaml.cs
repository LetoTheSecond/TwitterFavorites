﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Windows;
using System.Runtime.InteropServices;
using Newtonsoft.Json;
using Tweetinvi;
using Tweetinvi.Models;
using System.Threading;
using System.ComponentModel;

namespace TwitterArchiveDisplayer {

	struct Favorite{
		public String Name;
		public String ScreenName;
		public String Text;
		public List<String> Media;
		public String URL;
		public String Suffix;
	}

	struct Like {
		public String Text,
				Url,
				Suffix;
		public List<String> Media;
	}

	struct User : IComparable<User>{
		public long Id;
		public String Name, ScreenName;
		public List<Like> Likes;

		public int CompareTo(User other) {
			return Id.CompareTo(other.Id);
		}
	};

	/* New JSON Format
	 * [
	 * 	UserId : [
	 * 		TweedId : {
	 * 			Text: "",
	 * 			Url: "",
	 * 			Media: [],
	 * 			Suffix: ""
	 * 		}
	 * 	]
	 * ]
	 */

	public partial class MainWindow: Window {
		[DllImport("kernel32")]
		static extern bool AllocConsole();

		[DllImport("kernel32")]
		static extern bool FreeConsole();

		int idx = 0;
		List<User> Archive;
		DownloadingLikesProgressBar bar;
		BackgroundWorker LikesWorker;
		BackgroundWorker ImagesWorker;

		public MainWindow() {
			InitializeComponent();
			Archive = new List<User>();
			bar = new DownloadingLikesProgressBar();
			//this.ResizeMode = ResizeMode.NoResize;
			//SizeToContent = SizeToContent.WidthAndHeight;
		}

		private void DownloadLikes(object Sender, RoutedEventArgs args){
			bar.loadingbar.Minimum = 0;
			bar.loadingbar.Maximum = 100;
			bar.Show();
			LikesWorker = new BackgroundWorker {
				WorkerReportsProgress = true,
				WorkerSupportsCancellation = true
			};
			LikesWorker.DoWork += LikesWorker_DoWork;
			LikesWorker.ProgressChanged += LikesWorker_ProgressChanged;
			LikesWorker.RunWorkerAsync();
		}

		private void LikesWorker_ProgressChanged(object s,ProgressChangedEventArgs e) {
			bar.loadingbar.Value= e.ProgressPercentage;
		}

		private void LikesWorker_DoWork(object s, DoWorkEventArgs e) {
			BackgroundWorker worker = s as BackgroundWorker;
			Auth.SetUserCredentials(
				"sD956GlCAmBKgVSIJs7UohIoN",
				"1pitEmD5Fd1ALXcDXvHjgTHJxvseEhLjJg5JTESCIVHkWnUqJt",
				"213540951-NY59VmEyWDxim42zTGbzpcQ4iRudXxhcZe87BmfO",
				"E8Sel3vSsabWq5hq8oLYYnS686NXVDWwMa5PzmPSNA4Rm");
			
			//AllocConsole();
			int FavNum = Tweetinvi.User.GetAuthenticatedUser().FavouritesCount;
			int TotalFav = FavNum, Progress = 0 ;
			String ReturnJson = TwitterAccessor.ExecuteGETQueryReturningJson(
				"https://api.twitter.com/1.1/favorites/list.json?count=200"
			);

			while(FavNum > 0) {

				if(ReturnJson == null || ReturnJson.Length == 0) {
					Console.WriteLine("GET failed");
					return;
				}
				dynamic ReturnLikes = JsonConvert.DeserializeObject(ReturnJson);
				Console.WriteLine("{0:D}/{1:D}",FavNum,
					Tweetinvi.User.GetAuthenticatedUser().FavouritesCount);
				FavNum -= ReturnLikes.Count;
				Progress+= ReturnLikes.Count;
				long[] LikeIds = new long[ReturnLikes.Count];
				for(int i = 0;i < ReturnLikes.Count;i++) {
					LikeIds[i] = ReturnLikes[i].id;
				}
				Array.Sort(LikeIds);

				var LikeTweets = Tweet.GetTweets(LikeIds);
				foreach(ITweet Tweet in LikeTweets) {
					Like like = new Like() {
						Text = Tweet.Text,
						Url = Tweet.Url,
						Suffix = Tweet.Suffix,
						Media = new List<string>()
					};
					if(Tweet.Media.Count > 0) {
						foreach(var media in Tweet.Media) {
							like.Media.Add(media.MediaURL);
						}
					} else {
						like.Suffix = Tweet.Suffix;
						like.Media.Add("");
					}
					int i = Archive.FindIndex((User u) => u.Id == Tweet.CreatedBy.Id);
					if(i >= 0) {
						Archive[i].Likes.Add(like);
					} else {
						Archive.Add(new User {
							Id = Tweet.CreatedBy.Id,
							Name = Tweet.CreatedBy.Name,
							ScreenName = Tweet.CreatedBy.ScreenName,
							Likes = new List<Like>()
						});
						Archive[Archive.Count - 1].Likes.Add(like);
					}
				}

				worker.ReportProgress((int)(((float)Progress / TotalFav) * 100));
				if(FavNum > 0) {
					ReturnJson = TwitterAccessor.ExecuteGETQueryReturningJson(
						"https://api.twitter.com/1.1/favorites/list.json?" +
						"count=200&max_id=" + LikeIds[LikeIds.Length - 1]);
				}
			}
			File.WriteAllText("Likes.json",JsonConvert.SerializeObject(Archive,
				Formatting.Indented));
		}

		private void LoadImages(object sender,RoutedEventArgs e) {
			BackgroundWorker ImagesWorker = new BackgroundWorker {
				WorkerReportsProgress = true,
				WorkerSupportsCancellation = true
			};
			ImagesWorker.DoWork += ImagesWorker_Work;
			ImagesWorker.ProgressChanged += ImagesWorker_Progress;
			ImagesWorker.RunWorkerAsync();
		}

		private void ImagesWorker_Progress(object sender,ProgressChangedEventArgs e) {
			foreach (User user in Archive){
				
			}
		}

		private void ImagesWorker_Work(object sender,DoWorkEventArgs e) {
			return;
		}

		/*
		private void LoadImage() {
			this.Text.Text = likes[idx].Text;
			using(WebClient client = new WebClient()) {
				if (likes[idx].Media[0]==""){
					Img.Source = null;
					return;
				}
				var u = new Uri(likes[idx].Media[0]);
				Byte[] data = client.DownloadData(u);
				System.IO.MemoryStream s = new System.IO.MemoryStream(data);
				JpegBitmapDecoder decoder = new JpegBitmapDecoder(s,
					BitmapCreateOptions.PreservePixelFormat,BitmapCacheOption.Default);
				BitmapSource bsource = decoder.Frames[0];
				this.Img.Source = bsource;
				//this.Img.Width = bsource.Width;
				//this.Img.Height = bsource.Height;
			}
		}

		private void Window_Loaded(object sender,RoutedEventArgs e) {
			string file = File.ReadAllText("likes.json");
			likes = JsonConvert.DeserializeObject<List<Favorite>>(file);
			for(int i = 0;i < likes.Count;i++) {
				if(likes[i].Media.Count == 0) {
					likes.RemoveAt(i);
				}
			}
			this.ScreenName.Content = likes[idx].Name;
			LoadImage();
		}

		private void Button_Click(object sender,RoutedEventArgs e) {
			if(sender == this.LeftBtn) {
				if(idx > 0) {
					idx--;
				} else {
					return;
				}
			} else {
				if(idx < (likes.Count-1)) {
					idx++;
				} else {
					return;
				}
			}
			// this.ScreenName.Content = "[" + idx + "/" + (likes.Count - 1) + "] " + likes[idx].Name;
			this.ScreenName.Content = likes[idx].Name;
			LoadImage();
		}

		private void Img_MouseWheel(object sender,MouseWheelEventArgs e) {
			Img.Height += e.Delta * 0.5f;
			Img.Width += e.Delta * 0.5f;
		}
		*/

	}

}
